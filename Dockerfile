## Use latest ubuntu as main image
FROM ubuntu:latest

LABEL Author="Edward Andrey Portela <EPortela@lingotek.com>"

## Expose HTTP port
EXPOSE 80

## Environment variables
ENV DRUPAL_VERSION=9.1.x
ENV LINGOTEK_VERSION=3.4.x

## Copy files
COPY tzdata-debian.conf /tmp/tzdata-debian.conf
COPY .scripts /scripts
COPY databases /databases

## Install Apache and PHP
RUN chmod +x -R /scripts && apt-get update && apt-get install -y software-properties-common && add-apt-repository ppa:ondrej/php \
    && echo "deb http://archive.ubuntu.com/ubuntu focal main restricted universe multiverse" >> /etc/apt/sources.list \
    && apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends git apache2 sudo \
    debconf-utils && debconf-set-selections < /tmp/tzdata-debian.conf \
    && apt-get install -y --no-install-recommends sqlite3/focal \
    php7.4-dev php7.4-zip php7.4-curl php7.4-xmlrpc php7.4-gd php7.4-mysql php7.4-mbstring php7.4-xml libapache2-mod-php \
    phpunit php7.4-sqlite3 curl \
    && curl -sS https://getcomposer.org/installer -o composer-setup.php && php composer-setup.php \
    && mv composer.phar /usr/bin/composer && rm composer-setup.php \
    && sed -i "s,AllowOverride None,AllowOverride All,g" /etc/apache2/apache2.conf \
    && a2enmod rewrite \
    && git config --global user.email "integrations@Lingotek.com" && git config --global user.name "Drupal 8 Tester" \
    && export DRUPAL_VERSION=${DRUPAL_VERSION} && export LINGOTEK_VERSION=${LINGOTEK_VERSION} && \
    /scripts/create_ticket_env8-testing.sh \
    && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

ENTRYPOINT ["/scripts/entrypoint.sh"]
