#!/bin/bash

export image_name='lingotek-testing/drupal9.1'
./.create_image.sh && container_id=$(sudo docker ps -a | awk "(\$2 == \"$image_name\") { print \$1;} ") && \

patches_dir=`pwd`/patches

if [ ! -e "$patches_dir" ]; then
    mkdir -p $patches_dir
fi

if [ -z "${container_id}" ];
    then
        sudo docker run -it \
            --mount type=bind,source=$patches_dir,destination=/patches,readonly \
            --tmpfs /tmp:rw,nosuid,noexec,size=500m --cpus=0.000 --cpuset-cpus="0-3" \
            $image_name
    else
        sudo docker stop $container_id && sudo docker start -i --attach $container_id
fi
