#!/bin/bash

export image_id=$(sudo docker images | awk "(\$1 == \"$image_name\") { print \$3; }")

if [ -z "${image_id}" ]; then
        echo 'Building image...' && sudo docker build . -t $image_name;
    else
        echo "Image found with id: $image_id";
fi
