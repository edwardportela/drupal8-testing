#!/bin/bash
service apache2 start
if [ -z "${1+x}" ]; then
  exec /scripts/run_tests.sh
else
  exec "$1"
fi
