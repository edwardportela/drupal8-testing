#!/bin/bash

#### VARIABLES
export hostname=`hostname -i`
export drupal_version=$DRUPAL_VERSION
export lingotek_version=$LINGOTEK_VERSION #Version or branch to switch to if Lingotek repository was not downloaded previously
export hostname=`hostname -i`
export simpletest_base_url="\"http://$hostname/\""
export simpletest_db="\"sqlite://$hostname//tmp/test.sqlite\""
export replacement_string_one="s,<env name=\"SIMPLETEST_BASE_URL\" value=\"\"/>,<env name=\"SIMPLETEST_BASE_URL\" value=$simpletest_base_url/>,g"
export replacement_string_two="s,<env name=\"SIMPLETEST_DB\" value=\"\"/>,<env name=\"SIMPLETEST_DB\" value=$simpletest_db/>,g"

# Empty html directory
rm -r /var/www/html/*

#Copy Drupal Files
git clone https://git.drupalcode.org/project/drupal.git /var/www/html && cd /var/www/html
git checkout $drupal_version
composer install
composer run-script drupal-phpunit-upgrade
composer require 'acquia/cohesion:6.3.*' \
				 'drupal/block_field:dev-1.x' \
				 'drupal/bricks:dev-2.x' \
				 'drupal/entity_browser:dev-2.x' \
				 'drupal/entity_usage:dev-2.x' \
				 'drupal/group:dev-1.x' \
				 'drupal/metatag:*' \
				 'drupal/paragraphs:dev-1.x' \
				 'drupal/paragraphs_asymmetric_translation_widgets:dev-1.x' \
				 'drupal/pathauto:dev-1.x' \
				 'drupal/potx:dev-1.x' \
				 'drupal/search_api:dev-1.x' \
				 'drupal/workbench_moderation:dev-1.x' \
				 'drupal/layout_builder_st:dev-1.x' \
				 'drupal/layout_builder_at:dev-2.x' \
				 --prefer-stable --no-progress --no-suggest --working-dir /var/www/html
composer clear-cache
sed -i "$replacement_string_one" core/phpunit.xml.dist
sed -i "$replacement_string_two" core/phpunit.xml.dist
cp /var/www/html/sites/default/default.settings.php /var/www/html/sites/default/settings.php
mkdir -p /var/www/html/sites/default/files/
mkdir -p /var/www/html/sites/default/files/config_kAU3Y8BOOByqGHOvLffu2y6iUmpGv82RT6Q7_q8jg0WqApyl3Kl6hCPNdUkNF2rDenygDJeqWw/sync
mkdir /var/www/html/sites/simpletest
chown www-data:www-data -R /var/www/html
chmod a+w -R /var/www/html/sites/simpletest
chmod a+w -R /var/www/html/sites/default
chmod a+w -R /databases
chmod a+r -R /var/www/html
ln -s /var/www/html/vendor/drush/drush/drush /usr/local/bin

export hash_salt=`drush eval "echo Drupal\Component\Utility\Crypt::randomBytesBase64(55);"`

#Edit settings files
echo "\$databases['default']['default'] = array(
	'driver' => 'sqlite',
	'database' => '/databases/.ht.sqlite',
	'prefix' => '',
	'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\sqlite'
	);
\$config_directories['sync'] = 'sites/default/files/config_kAU3Y8BOOByqGHOvLffu2y6iUmpGv82RT6Q7_q8jg0WqApyl3Kl6hCPNdUkNF2rDenygDJeqWw/sync';
\$settings['config_sync_directory'] = 'sites/default/files/config_kAU3Y8BOOByqGHOvLffu2y6iUmpGv82RT6Q7_q8jg0WqApyl3Kl6hCPNdUkNF2rDenygDJeqWw/sync';
\$settings['hash_salt'] = '$hash_salt';
\$settings['update_free_access'] = TRUE;
	" >> /var/www/html/sites/default/settings.php

#Create link for Lingotek repository
git clone https://git.drupalcode.org/project/lingotek.git /var/www/html/modules/lingotek && cd /var/www/html/modules/lingotek
git checkout $lingotek_version
