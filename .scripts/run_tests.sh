#!/bin/bash
export hostname=`hostname -i`

cd /var/www/html/modules/lingotek

echo 'Resetting Lingotek module'
git checkout -- . && git clean -df

echo 'Pulling latest changes for Lingotek module'
git pull

echo 'Patching Lingotek module..'

export success=false

files=$( ls /patches )
if [ -z "${files}" ]; then
    echo 'No patches found. Testing without patches' && export success=true
fi

for file in $files; do
    export success=false
    git apply -v /patches/$file && export success=true
done

if [ -f "/tmp/test.sqlite" ]; then
    echo 'Deleting previous test runs'
    rm -rf /tmp/test.sqlite
fi

rm -rf /var/www/html/sites/simpletest/*

cd /var/www/html

echo 'Pulling latest changes for Drupal'
git stash && git pull && git stash apply && git stash drop && composer install

echo 'Running tests'

if [ "$success" = "true" ]; then sudo -u www-data php core/scripts/run-tests.sh --color --keep-results --suppress-deprecations \
    --types "Simpletest,PHPUnit-Unit,PHPUnit-Kernel,PHPUnit-Functional" --concurrency "32" --repeat "1" \
    --directory modules/lingotek --sqlite "/tmp/test.sqlite" --url "http://$hostname/" \
    --dburl "sqlite://$hostname//tmp/test.sqlite" --verbose --non-html
    else
        echo 'One or more patches failed to apply. Aborting test.'
fi

if [ ! -f "/tmp/dont.run.bash" ]; then
    touch /tmp/dont.run.bash
    exec /bin/bash
fi
