# Drupal 8 Testing Environment
## How to use
1. Put any patches for the lingotek module inside of the `patches` directory (Remove any patches from the directory to test without patches)
2. Run the following command from inside the current foder: `./run_image.sh`

## Notes
- An image will be built the first time you run the command. This can take a few minutes but the image is reused every time the command `./run_image.sh` is ran.
- Add or remove patches from the `patches` directory as needed before running the `./run_image.sh` command. The tests may be cancelled at any time by hitting `CTRL + C`. This will drop you to a shell inside the container.
- The `patches` directory is bound to the host file system so any changes you make in the host will be reflected in the container but they are read-only inside the container.
- To rerun the tests while inside the container run the following command from the shell: `/scripts/run_tests.sh`
- The Drupal code can be found inside the container at `/var/www/html`. It is a `git` repository.
- The Lingotek module code can be found inside the container at `/var/www/html/modules/lingotek`. It is also a `git` repository.
- No file editor is included but one can be installed from the repositories by using a command such as the following from a shell inside the container: `apt update && apt install vim -y`
- While the docker container is running, a Drupal 8 site can be accessed using the container's IP address. For example http://172.17.0.2/. The default username and password for the site are both `admin`
